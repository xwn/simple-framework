﻿// See https://aka.ms/new-console-template for more information
using System.Linq.Expressions;
using Simple.Console;
using Simple.Console.Test;
using Simple.Core;
using Simple.Core.Helper;

ConfigHelper.Init(new string[] { "appsettings.json" }, false, true);

TestHtmlPdf.TestAsync();